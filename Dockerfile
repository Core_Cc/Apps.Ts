FROM circleci/node:latest-browsers
          
WORKDIR /usr/src/app
USER root
COPY package.json ./
RUN npm config set registry https://registry.npm.taobao.org


RUN npm i node-sass --sass_binary_site=https://npm.taobao.org/mirrors/node-sass/
RUN npm install

COPY ./ ./



CMD ["npm", "run", "build"]