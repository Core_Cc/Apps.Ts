import md5 from 'md5'
/**
 * 工具类
 */
export class BaseUtil {

    static callPhone(phone: string) {
        uni.makePhoneCall({
            phoneNumber: phone
        })
    }

    static jsonToUrl<T>(o: T) {
        let arr: Array<string> = [];
        for (let i in o) {
            if (i != "constructor") arr.push(`${i}=${o[i]}`);
        }
        return arr.join("&");
    }

    /**
     * http请求不带contentType
     * @param url
     * @param method
     * @param data
     */
    static superHttp(url: string, method: any, data?: any, contentType?: string): any {
       
        if (!!contentType) {
            let header: any = {}
            header['token'] = contentType
            return uni.request({
                url, method, data, header
            })
        }

        return uni.request({
            url, method, data
        })
    }

    static toH2ex(t: string) {
        t = md5(t, {

        }).toUpperCase()
        var Password: string = "";
        let i: number = 0;
        t.split("").map((x: string) => {
            Password += x;
            i++;
            if (i == 2) {
                Password += "-";
                i = 0;
            }
        });
        Password = Password.substring(0, Password.length - 1);
        return Password;
    }


    /**
     * 获取环境变量url
     */
    static getBaseUrl() {
        return process.env.VUE_APP_URL
    }


    static showLoading() {
        uni.showLoading({
            title: '请稍后',
            mask: true
        })
    }
}