import { LoginDto } from '@/core/dto/LoginDto';
import { ILog } from '../ILog';
import { BaseUtil } from '@/core/util/BaseUtil';

export class LogAppService implements ILog {
    print(req: LoginDto): void {
        console.log(BaseUtil.jsonToUrl(req))
    }


}