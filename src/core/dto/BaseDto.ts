
export class BaseDto {

    // 8a7175dc-f7a4-4f62-8bc7-6cfff4520f4e
    readonly companyKey: string = "8a7175dc-f7a4-4f62-8bc7-6cfff4520f4e";

    readonly oem: string = "1582193362818";

    sid: string = uni.getStorageSync('sid')

    mallSid: string = !!uni.getStorageSync('user').mallSid ? JSON.parse(uni.getStorageSync('user').mallSid).sid : ''

    constructor() {

    }

}
