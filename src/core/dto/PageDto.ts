import { BaseDto } from './BaseDto';

export class PageDto extends BaseDto {

    pageSize: number = 10;
    currentPage: number = -1;

    pageIndex: number = this.currentPage


}