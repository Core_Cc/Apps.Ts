import { BaseDto } from './BaseDto';


export class LoginDto extends BaseDto {
    /**
     * 用户名
     */
    userName!: string;

    /**
     * 登陆密码
     */
    passWord!: string;
}