///<reference path="../../main.d.ts" />
import {Vue, Component} from 'vue-property-decorator'

/**
 * mixins
 */
@Component
export default class extends Vue {

    onShow() {
        // #ifdef H5
        let i: number = 0;
        this.$nextTick(() => {
            let ele = <StyleElement>document.getElementsByTagName('uni-app')[0];
            ele.classList.add('h5_enter')
            setTimeout(() => {
                ele.classList.remove('h5_enter')
            }, 1000);
        }) 
        // #endif
		
		
		
		
    }


    showModal(content: string, ok?: () => void, cancel?: Function) {
        uni.showModal({
            title: '提示',
            content,
            success: res => {
                if (res.confirm && !!ok) {
                    ok()
                } else if (!res.confirm && !!cancel) {
                    cancel()
                }
            }
        });
    }

    /**
     * 兼容app wx小程序 h5的toast
     */
    toast(msg: string, complete?: any) {
        // #ifdef APP-PLUS
        plus.nativeUI.toast(msg)
        // #endif
        // #ifdef H5
        uni.showToast({
            title: msg,
            icon: 'none',
            complete: complete,
            mask: true
        })
        // #endif
        // #ifdef MP-WEIXIN
        uni.showToast({
            title: msg,
            icon: 'none',
            complete,
            mask: true
        })
        // #endif
    }

    /**
     * 跳转页面 可返回
     * @param url
     */
    reLaunch(url: string) {
        // #ifdef APP-PLUS
        uni.reLaunch({
            url
        })
        return
        // #endif
        uni.reLaunch({
            url
        })
    }

    /**
     * 跳转页面 可返回
     * @param url
     */
    redirectTo(url: string) {
        // #ifdef APP-PLUS
        uni.redirectTo({
            url
        })
        return
        // #endif
        uni.redirectTo({
            url
        })
    }

    /**
     * 跳转页面 可返回
     * @param url
     */
    nav(url: string) {
        // #ifdef APP-PLUS
        uni.navigateTo({
            url,
            animationType:"slide-in-top",
            animationDuration:500
        })
        return
        // #endif
        uni.navigateTo({
            url
        })
    }
}