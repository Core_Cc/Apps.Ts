
declare class StyleElement extends Element {
    style: {
        height: string;
        marginLeft: string;
        left: string;
        display: string;
        opacity: number;
        animation: string;
        marginTop: string;
    }
}




